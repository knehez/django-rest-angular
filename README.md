# Telepítés #

### Git repositoryból clone-ozás
git clone https://[username]@bitbucket.org/knehez/django-rest-angular.git

### Virtuális environment telepítése

pip install virtualenv
cd django-rest-angular
virtualenv venv

### Virtuális environment indítása

venv\Scripts\activate

A prompth megváltozik (venv)-re

### Kiegészítők telepítése

Ez a lépés csak az első telepítéskor kell

pip install -r requirements.txt

### Django inicializálása

Az alap adatbázis létrehozása:
manage.py migrate

Admin user létrehozása, mert egyébként nem tudjuk elérni az admin felületet

manage.py createsuperuser

#### Szerver indítása

manage.py runserver

### YAML leírás

http://www.yaml.org/spec/1.2/spec.html