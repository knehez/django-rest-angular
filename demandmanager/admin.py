from django.contrib import admin
from demandmanager.models import DemandItem, DemandState, DemandTransactionHistory

@admin.register(DemandItem)
class DemandItemAdmin(admin.ModelAdmin):
    pass


@admin.register(DemandState)
class DemandStateAdmin(admin.ModelAdmin):
    pass


@admin.register(DemandTransactionHistory)
class DemandTransactionHistoryAdmin(admin.ModelAdmin):
    list_filter = ('item',)