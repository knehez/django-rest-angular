from django.apps import AppConfig


class DemandmanagerConfig(AppConfig):
    name = 'demandmanager'
