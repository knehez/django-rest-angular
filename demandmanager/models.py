from django.db import models
from django_fsm import FSMField
from django.utils.timezone import now


class DemandState(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    label = models.CharField(max_length=255)

    class Meta:
        verbose_name = "DemandState"

    def __str__(self):
        return self.label

class DemandItem(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    vendor = models.CharField(max_length = 100, blank=True, null=True)
    vendor_email = models.EmailField(blank=True, null=True)
    price = models.DecimalField(max_digits=10, decimal_places=0, blank=True, null=True)
    rejected_reason = models.TextField(blank=True, null=True)
    inventory_code = models.CharField(max_length=20, blank=True, null=True)
    user = models.CharField(max_length=20, blank=True, null=True)

    state = FSMField(DemandState, default='demand', protected=False)

    class Meta:
        permissions = (
            ("demand_approval", "Can approve demands"),
        )

    def __str__(self):
        return self.title

class DemandTransactionHistory(models.Model):
    timestamp = models.DateTimeField(default=now)
    success = models.BooleanField(default=False)
    start_state = models.CharField(max_length=30)
    end_state = models.CharField(max_length=30)
    item = models.ForeignKey(DemandItem)

    def __str__(self):
        return '%s --- start state: %s --> %s (success: %s)' % (
            self.timestamp, self.start_state, self.end_state, self.success)
