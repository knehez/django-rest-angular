from rest_framework import serializers
from demandmanager.models import DemandItem, DemandState


class DemandItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = DemandItem


class DemandStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DemandState