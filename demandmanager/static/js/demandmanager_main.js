var app = angular.module('rest-demandmanager', ['ui.router', 'ui.bootstrap', 'ngCookies']);

var loggedIn = false;

app.constant('API_URL', window.location.origin + '/demandmanager/api/demands/');
app.constant('REST_URL', window.location.origin + '/demandmanager/changestate/');
app.constant('AUTH_URL', window.location.origin + '/demandmanager/authenticate/');

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'

    $('[data-toggle="tooltip"]').tooltip();

    String.prototype.format = function () {
        var formatted = this;
        for (var arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }
        return formatted;
    };

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '/static/templates/home_demand.html',
            controller: 'MainCtrl'
        })
        .state('login', {
            url: "/login",
            templateUrl: '/static/templates/login_demand.html',
            controller: 'MainCtrl'
        })
        .state('edit-demand', {
            url: "/edit",
            templateUrl: '/static/templates/edit_demand.html',
            controller: 'MainCtrl'
        })
        .state('add-demand', {
            url: "/add",
            templateUrl: '/static/templates/add_demand.html',
            controller: 'MainCtrl'
        });

    $urlRouterProvider.otherwise('/');
})
    .run(function ($location, $cookies, $state, $rootScope) {
        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
            var isLogin = toState.name === "login";
            if (isLogin || loggedIn) {
                return; // no need to redirect 
            }
            // redirect of not logged in
            if ($cookies['sessionid'] == undefined) {
                e.preventDefault();
                $state.go('login');
            }
        })
    });

app.controller('MainCtrl', function ($scope, $rootScope, Demands, $state, $filter) {
    $scope.newDemand = {};

    $scope.itemsPerPage = 5;
    $scope.currentPage = 1;

    $scope.UserLogin = function () {
        $scope.dataLoading = true;
        console.log($scope.login.username, $scope.login.password);
        Demands.authenticate({ username: $scope.login.username, password: $scope.login.password }).then(function successCallback(response) {
            loggedIn = true;
            $state.go('home');
        }, function errorCallback(response) {
            // error
            $("#modalLoginError").modal();
        });
    }

    $scope.showInState = function (showStates) {
        currentModel = $state.current.url === '/edit' ? $rootScope.currentDemand : $scope.newDemand   
        for(const state of showStates) {
            if(state === currentModel.state) {
                return true;
            }
        }
        return false;
    }

    $scope.addDemand = function () {
        Demands.addOne($scope.newDemand)
            .then(function (res) {
                // redirect to homepage once added
                $state.go('home');
            });
    };

    $scope.editDemand = function (demand) {
        Demands.update(demand).then(function (res) {
            // redirect to homepage once added
            $state.go('home');
        });
    }

    $scope.editCurrent = function (demand) {
        $rootScope.currentDemand = demand;
        $state.go('edit-demand');
    }

    $scope.toggleFinished = function (demand) {
        Demands.update(demand);
    };

    $scope.deleteDemand = function (id) {
        Demands.delete(id);
        // update the list in ui
        $scope.demands = $scope.demands.filter(function (demand) {
            return demand.id !== id;
        })
    };

    $scope.changeState = function (id, state) {
        var currentId = id;
        Demands.changeState(id, state).then(function successCallback(response) {
            //$scope.demands[currentId].state = response.data;
            $scope.demands = $scope.demands.filter(function (demand) {
                if (demand.id == id) {
                    demand.state = response.data;
                }
                return true;
            })
        }, function errorCallback(response) {
            // error
            $("#modalNotAllowed").modal();
        });
    };

    $scope.paginatedListToDisplay = function () {
        $scope.filteredItems = $filter('filter')($scope.demands, $scope.search);
        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;
        $scope.paginatedDemands = $scope.filteredItems.slice(begin, end);
    };

    Demands.all().then(function (res) {
        $scope.filteredItems = $scope.demands = res.data;
        $scope.paginatedListToDisplay();
    });
});

app.service('Demands', function ($http, API_URL, REST_URL, AUTH_URL) {
    var Demands = {};

    Demands.all = function () {
        return $http.get(API_URL);
    };

    Demands.update = function (updatedDemand) {
        return $http.put("{0}{1}/".format(API_URL, updatedDemand.id), updatedDemand);
    };

    Demands.delete = function (id) {
        return $http.delete("{0}{1}/".format(API_URL, id));
    };

    Demands.addOne = function (newDemand) {
        return $http.post(API_URL, newDemand)
    };

    Demands.changeState = function (id, state) {
        return $http.get("{0}{1}/{2}".format(REST_URL, id, state));
    }

    Demands.authenticate = function (user) {
        return $http.post(AUTH_URL, user);
    };

    return Demands;
});




