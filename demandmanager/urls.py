from django.conf.urls import include, url
from rest_framework import routers
from . import views
from django.conf import settings
from django.conf.urls.static import static

rest_router = routers.DefaultRouter()
rest_router.register(r'demands', views.DemandViewSet, base_name='demands')
rest_router.register(r'states', views.DemandStateViewSet, base_name='demandstates')

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^authenticate/', views.authenticate),
    url(r'^api/', include(rest_router.urls)),
    url(r'^changestate/(?P<pk>[0-9]+)/(?P<new_state>[a-z_]+)$', views.change_state),
    url(r'^getstates/(?P<model>([A-z]|[a-z])+)/', views.get_states),

]