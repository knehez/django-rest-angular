from django.shortcuts import render, get_object_or_404
from django_fsm import can_proceed, has_transition_perm
from rest_framework import exceptions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets, status
from rest_framework.exceptions import PermissionDenied
from demandmanager.models import DemandItem, DemandState, DemandTransactionHistory
from demandmanager.serializers import DemandStateSerializer, DemandItemSerializer

# Home route to send template to angular
def index(request):
    return render(request, 'demandmanager_base.html')

@api_view(['POST'])
def authenticate(request):
    username = request.data['username']
    password = request.data['password']
    if not username:
        return None
    from django.contrib.auth import authenticate, login    
    user = authenticate(username=username, password=password)
    if user is not None:
        login(request, user)
        request.session['loggedin'] = 'true'
        return Response({'user': username}, status=status.HTTP_200_OK)
    else:
        raise exceptions.AuthenticationFailed('No such user')

class DemandViewSet(viewsets.ModelViewSet):
    queryset = DemandItem.objects.all()
    serializer_class = DemandItemSerializer


class DemandStateViewSet(viewsets.ModelViewSet):
    queryset = DemandState.objects.all()
    serializer_class = DemandStateSerializer

def save_state_change(item, old_state, new_state, success):
    p = DemandTransactionHistory.objects.create(success=success, start_state=old_state, end_state=new_state, item=item)
    p.save()

@api_view(['GET'])
def get_states(request, model):
    transitions = list(DemandItem().get_all_state_transitions())

    available_transitions = [[(t.name, t.custom) for t in transitions]]
    ordered_transactions = sorted(available_transitions[0], key=lambda item: item[1]['order'])
    return Response(ordered_transactions, status=status.HTTP_200_OK)


@api_view(['GET'])
def change_state(request, pk, new_state):
    demanditem = get_object_or_404(DemandItem, pk=pk)

    old_state = demanditem.state
    print("user:" + request.user.username)
    if not can_proceed(getattr(demanditem, new_state)) or not has_transition_perm(getattr(demanditem, new_state), request.user):
        save_state_change(demanditem, old_state, new_state, success=False)
        raise PermissionDenied

    getattr(demanditem, new_state)()

    demanditem.save()

    save_state_change(demanditem, old_state, new_state, success=True)

    return Response(demanditem.state, status=status.HTTP_200_OK)