from django.contrib import admin
from taskmanager.models import TaskItem, TaskTransactionHistory, TaskState


@admin.register(TaskItem)
class TaskItemAdmin(admin.ModelAdmin):
    pass


@admin.register(TaskState)
class TaskStateAdmin(admin.ModelAdmin):
    pass


@admin.register(TaskTransactionHistory)
class TaskTransactionHistoryAdmin(admin.ModelAdmin):
    list_filter = ('item',)