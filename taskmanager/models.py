from django.db import models
from django_fsm import FSMField
from django.utils.timezone import now


class TaskState(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    label = models.CharField(max_length=255)

    class Meta:
        verbose_name = "TaskState"

    def __str__(self):
        return self.label


class TaskItem(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    is_finished = models.BooleanField(default=False)

    state = FSMField(TaskState, default='draft', protected=False)

    class Meta:
        permissions = (
            ("taskitem_approval", "Can approve tasks"),
        )

    def __str__(self):
        return self.title


class TaskTransactionHistory(models.Model):
    timestamp = models.DateTimeField(default=now)
    success = models.BooleanField(default=False)
    start_state = models.CharField(max_length=30)
    end_state = models.CharField(max_length=30)
    item = models.ForeignKey(TaskItem)

    def __str__(self):
        return '%s --- start state: %s --> %s (success: %s)' % (
            self.timestamp, self.start_state, self.end_state, self.success)
