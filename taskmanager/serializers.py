from rest_framework import serializers
from taskmanager.models import TaskItem, TaskState


class TaskItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskItem


class TaskStateSerializer(serializers.ModelSerializer):
    class Meta:
        model = TaskState