from django.db.models.signals import class_prepared
from django_fsm import transition
import yaml

fixture = []


def add_transitions(sender, **kwargs):
    global fixture
    if len(fixture) == 0:
        with open('taskmanager/fixtures/transitions.yaml', 'r') as f:
            fixture = yaml.load(f)

    def make_empty_method(name):
        def _method(self):
            pass

        _method.__name__ = name
        return _method

    def add_transition(item):
        method_name = item['function']
        # when running tests: in case of fake models do not add transitions
        if 'fake' in str(sender):
            return
        trans = transition(sender._meta.get_field('state'), source=item['transition']['source'],
                           target=item['transition']['target'], custom=item['custom'],
                           permission=item['transition']['permission'])
        # make a decorator by hand
        method = trans(make_empty_method(method_name))
        setattr(sender, method_name, method)

    [add_transition(item) for item in fixture if item['model'] == sender.__name__]


class_prepared.connect(add_transitions)