var app = angular.module('rest-task', ['ui.router', 'ui.bootstrap', 'ngCookies']);

var loggedIn = false;

app.constant('API_URL', window.location.origin + '/taskmanager/api/tasks/');
app.constant('REST_URL', window.location.origin + '/taskmanager/changestate/');
app.constant('AUTH_URL', window.location.origin + '/taskmanager/authenticate/');

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'

    $('[data-toggle="tooltip"]').tooltip();

    String.prototype.format = function () {
        var formatted = this;
        for (var arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }
        return formatted;
    };

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '/static/templates/home_task.html',
            controller: 'MainCtrl'
        })
        .state('login', {
            url: "/login",
            templateUrl: '/static/templates/login_task.html',
            controller: 'MainCtrl'
        })
        .state('edit-task', {
            url: "/edit",
            templateUrl: '/static/templates/edit_task.html',
            controller: 'MainCtrl'
        })
        .state('add-task', {
            url: "/add",
            templateUrl: '/static/templates/add_task.html',
            controller: 'MainCtrl'
        });

    $urlRouterProvider.otherwise('/');
})
    .run(function ($location, $cookies, $state, $rootScope) {
        $rootScope.$on('$stateChangeStart', function (e, toState, toParams, fromState, fromParams) {
            var isLogin = toState.name === "login";
            if (isLogin || loggedIn) {
                return; // no need to redirect 
            }
            // redirect of not logged in
            if ($cookies['sessionid'] == undefined) {
                e.preventDefault();
                $state.go('login');
            }
            return;
        })
    });

app.controller('MainCtrl', function ($scope, $rootScope, Tasks, $state, $filter) {
    $scope.newTask = {};
    $scope.login = {};

    $scope.itemsPerPage = 5;
    $scope.currentPage = 1;

    $scope.UserLogin = function () {
        $scope.dataLoading = true;
        console.log($scope.login.username, $scope.login.password);
        Tasks.authenticate({ username: $scope.login.username, password: $scope.login.password }).then(function successCallback(response) {
            loggedIn = true;
            $state.go('home');
        }, function errorCallback(response) {
            // error
            $("#modalLoginError").modal();
        });
    }

    $scope.addTask = function () {
        Tasks.addOne($scope.newTask)
            .then(function (res) {
                // redirect to homepage once added
                $state.go('home');
            });
    };

    $scope.editTask = function (task) {
        Tasks.update(task).then(function (res) {
            // redirect to homepage once added
            $state.go('home');
        });
    }

    $scope.editCurrent = function (task) {
        $rootScope.currentTask = task;
        console.log($scope.currentTask);
        $state.go('edit-task');
    }

    $scope.toggleFinished = function (task) {
        Tasks.update(task);
    };

    $scope.deleteTask = function (id) {
        Tasks.delete(id);
        // update the list in ui
        $scope.tasks = $scope.tasks.filter(function (task) {
            return task.id !== id;
        })
    };

    $scope.changeState = function (id, state) {
        var currentId = id;
        Tasks.changeState(id, state).then(function successCallback(response) {
            //$scope.tasks[currentId].state = response.data;
            $scope.tasks = $scope.tasks.filter(function (task) {
                if (task.id == id) {
                    task.state = response.data;
                }
                return true;
            })
        }, function errorCallback(response) {
            // error
            $("#modalNotAllowed").modal();
        });
    };

    $scope.paginatedListToDisplay = function () {
        $scope.filteredItems = $filter('filter')($scope.tasks, $scope.search);
        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;
        $scope.paginatedTasks = $scope.filteredItems.slice(begin, end);
    };

    Tasks.all().then(function (res) {
        $scope.filteredItems = $scope.tasks = res.data;
        $scope.paginatedListToDisplay();
    });
});

app.service('Tasks', function ($http, API_URL, REST_URL, AUTH_URL) {
    var Tasks = {};

    Tasks.all = function () {
        return $http.get(API_URL);
    };

    Tasks.update = function (updatedTask) {
        return $http.put("{0}{1}/".format(API_URL, updatedTask.id), updatedTask);
    };

    Tasks.delete = function (id) {
        return $http.delete("{0}{1}/".format(API_URL, id));
    };

    Tasks.addOne = function (newTask) {
        return $http.post(API_URL, newTask)
    };

    Tasks.changeState = function (id, state) {
        return $http.get("{0}{1}/{2}".format(REST_URL, id, state));
    }

    Tasks.authenticate = function (user) {
        return $http.post(AUTH_URL, user);
    }
    return Tasks;
});




