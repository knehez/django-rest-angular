from datetime import datetime
from django.test import override_settings
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium import webdriver
from taskmanager.models import TaskItem
from selenium.webdriver.support import expected_conditions as EC
from django.conf import settings
import os

@override_settings(DEBUG=True)
class MySeleniumTests(StaticLiveServerTestCase):
    @classmethod
    def setUpClass(cls):
        super(MySeleniumTests, cls).setUpClass()
        settings.CSRF_COOKIE_SECURE = False
        settings.SESSION_COOKIE_SECURE = False
        cls.selenium = webdriver.Chrome(r'C:/Users/knehez/AppData/Roaming/npm/chromedriver.cmd')
        #cls.selenium = webdriver.PhantomJS(r'C:/Users/knehez/AppData/Roaming/npm/phantomjs.cmd')
        cls.selenium.implicitly_wait(0)

    @classmethod
    def tearDownClass(cls):
        cls.selenium.quit()
        super(MySeleniumTests, cls).tearDownClass()

    @classmethod
    def make_screenshot(self):
        now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        self.selenium.save_screenshot('screenshots/screen_%s.png' % now)

    @classmethod
    def printBrowserLogs(self):
        for entry in self.selenium.get_log('browser'):
            print(entry)

    def test_new_item_add(self):
        from seleniumlogin import force_login
        from django.contrib.auth import get_user_model
        User = get_user_model()
        user = User.objects.create_user(username='myuser', password='password', is_superuser=True)
        force_login(user, self.selenium, self.live_server_url)
        """
        add a new item and check the list
        """
        self.selenium.get('%s%s' % (self.live_server_url, '/taskmanager'))

        self.printBrowserLogs()

        WebDriverWait(self.selenium, 10).until(lambda s: s.find_element_by_id("add_new_item")).is_displayed()
        self.selenium.find_element_by_id("add_new_item").click()

        WebDriverWait(self.selenium, 10).until(lambda s: s.find_element_by_id("title")).is_displayed()
        title = self.selenium.find_element_by_id("title")
        title.send_keys("new test title")
        description = self.selenium.find_element_by_id("description")
        description.send_keys("new test description")

        self.selenium.find_element_by_id("add").click()

        WebDriverWait(self.selenium, 10).until(lambda s: s.find_element_by_id("add_new_item")).is_displayed()
        rows = len(self.selenium.find_elements_by_xpath("//table[@id='taskitemtable']/tbody/tr"))
        self.printBrowserLogs()
        self.assertEqual(rows, 2)

    def test_new_item_cancel(self):
        return
        """
        add a new item and cancel adding
        """
        self.selenium.get('%s%s' % (self.live_server_url, '/taskmanager'))

        WebDriverWait(self.selenium, 10).until(lambda s: s.find_element_by_id("add_new_item")).is_displayed()
        current_rows = len(self.selenium.find_elements_by_xpath("//table[@id='taskitemtable']/tbody/tr"))
        self.selenium.find_element_by_id("add_new_item").click()

        WebDriverWait(self.selenium, 10).until(lambda s: s.find_element_by_id("title")).is_displayed()
        title = self.selenium.find_element_by_id("title")
        title.send_keys("other title")
        description = self.selenium.find_element_by_id("description")
        description.send_keys("my description description description")

        self.selenium.find_element_by_id("cancel").click()

        WebDriverWait(self.selenium, 10).until(lambda s: s.find_element_by_id("add_new_item")).is_displayed()
        rows = len(self.selenium.find_elements_by_xpath("//table[@id='taskitemtable']/tbody/tr"))
        # self.printBrowserLogs()
        self.assertEqual(rows, current_rows)

    def test_next_available_state(self):
        return
        TaskItem.objects.create(title='item1', description='desc1')
        TaskItem.objects.create(title='item2', description='desc2')
        self.selenium.get('%s%s' % (self.live_server_url, '/taskmanager'))

        WebDriverWait(self.selenium, 10).until(EC.presence_of_element_located((By.ID,"add_new_item"))).is_displayed()

        rows = len(self.selenium.find_elements_by_xpath("//table[@id='taskitemtable']/tbody/tr"))
        self.assertEqual(rows, 2)

        # check inital_state = draft
        state_label = self.selenium.find_element_by_id("lbl.state.0")
        self.assertEqual(state_label.text, 'draft')

        # click dropdown menu and click approve button
        self.selenium.find_element_by_id("btn.dropdown-toggle.0").click()
        WebDriverWait(self.selenium, 10).until(
            EC.element_to_be_clickable((By.ID, 'btn.state.approve.0'))).click()

        # check if approved is visible
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, 'lbl.state.0'), 'approved'))

        # try to set wrong state
        self.selenium.find_element_by_id("btn.dropdown-toggle.0").click()
        WebDriverWait(self.selenium, 10).until(
            EC.element_to_be_clickable((By.ID, 'btn.state.finish.0'))).click()

        # check if 'approved' is visible
        WebDriverWait(self.selenium, 10).until(
            EC.text_to_be_present_in_element((By.ID, 'lbl.state.0'), 'approved'))
        self.make_screenshot()