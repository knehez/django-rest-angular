from django.test import TestCase
from taskmanager.models import TaskItem
from django_fsm import can_proceed


class FiniteMachineTests(TestCase):
    def test_item_can_change_state(self):
        item = TaskItem("My item")
        self.assertEqual(can_proceed(item.approve), True)