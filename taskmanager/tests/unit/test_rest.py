from django.test import TestCase, Client
import yaml


def setUpModule():
    print('Finite State Machine Setup')


class FiniteMachineTests(TestCase):
    def setUp(self):
        self.client = Client(enforce_csrf_checks=True)
        with open('taskmanager/fixtures/transitions.yaml', 'r') as f:
            self.fixture = yaml.load(f)

    def test_get_configured_states(self):
        response = self.client.get('/taskmanager/getstates/TaskItem/')
        # check the same elements in the state list as in stored states
        self.assertEqual(response.status_code, 200)
        # compare states with stored in fixtures
        state_names = tuple([[item[0] for item in response.data]])
        self.assertEqual(set(state_names[0]), set(tuple([item['function'] for item in self.fixture])))

    def test_add_new(self):
        response = self.client.post('/taskmanager/api/tasks/', {'title': 'new title', 'description': 'new description'},
                                    HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.assertEqual(response.status_code, 201)