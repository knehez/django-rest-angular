from django.shortcuts import render, get_object_or_404
from django_fsm import can_proceed, has_transition_perm
from rest_framework import exceptions
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import status
from rest_framework import viewsets, status
from taskmanager.models import TaskItem, TaskState, TaskTransactionHistory
from taskmanager.serializers import TaskItemSerializer, TaskStateSerializer
from rest_framework.exceptions import PermissionDenied
from django.views.decorators.csrf import csrf_exempt

# Home route to send template to angular
def index(request):
    return render(request, 'taskmanager_base.html')

@api_view(['POST'])
def authenticate(request):
    username = request.data['username']
    password = request.data['password']
    if not username:
        return None
    from django.contrib.auth import authenticate, login    
    user = authenticate(username=username, password=password)
    if user is not None:    
        login(request, user)
        request.session['loggedin'] = 'true'
        return Response({'user': username}, status=status.HTTP_200_OK)
    else:
        raise exceptions.AuthenticationFailed('No such user')

class TaskViewSet(viewsets.ModelViewSet):
    queryset = TaskItem.objects.all()
    serializer_class = TaskItemSerializer

class TaskStateViewSet(viewsets.ModelViewSet):
    queryset = TaskState.objects.all()
    serializer_class = TaskStateSerializer

def save_state_change(item, old_state, new_state, success):
    p = TaskTransactionHistory.objects.create(success=success, start_state=old_state, end_state=new_state, item=item)
    p.save()

@api_view(['GET'])
def get_states(request, model):
    transitions = list(TaskItem().get_all_state_transitions())

    available_transitions = [[(t.name, t.custom) for t in transitions]]
    ordered_transactions = sorted(available_transitions[0], key=lambda item: item[1]['order'])
    return Response(ordered_transactions, status=status.HTTP_200_OK)


@api_view(['GET'])
def change_state(request, pk, new_state):
    taskitem = get_object_or_404(TaskItem, pk=pk)

    old_state = taskitem.state
    print("user:" + request.user.username)
    if not can_proceed(getattr(taskitem, new_state)) or not has_transition_perm(getattr(taskitem, new_state), request.user):
        save_state_change(taskitem, old_state, new_state, success=False)
        raise PermissionDenied

    getattr(taskitem, new_state)()

    taskitem.save()

    save_state_change(taskitem, old_state, new_state, success=True)

    return Response(taskitem.state, status=status.HTTP_200_OK)