from django.contrib import admin

# Register your models here.
from todolist_server.models import TodoItem, TransactionHistory, State


@admin.register(TodoItem)
class TodoItemAdmin(admin.ModelAdmin):
    pass


@admin.register(State)
class StateAdmin(admin.ModelAdmin):
    pass


@admin.register(TransactionHistory)
class TransactionHistoryAdmin(admin.ModelAdmin):
    list_filter = ('item',)

# read only transactionlog


class ReadOnlyTransactionHistory(TransactionHistory):
    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        pass

@admin.register(ReadOnlyTransactionHistory)
class TransactionHistoryReadOnlyAdmin(admin.ModelAdmin):
    list_filter = ('item',)