from django.apps import AppConfig


class TodolistServerConfig(AppConfig):
    name = 'todolist_server'
