from django.db import models
from django_fsm import FSMField
from django.utils.timezone import now


class State(models.Model):
    id = models.CharField(primary_key=True, max_length=50)
    label = models.CharField(max_length=255)

    class Meta:
        verbose_name = "State"

    def __str__(self):
        return self.label


class TodoItem(models.Model):
    title = models.CharField(max_length=100)
    description = models.TextField()
    is_finished = models.BooleanField(default=False)

    state = FSMField(State, default='draft', protected=False)

    class Meta:
        permissions = (
            ("todoitem_approval", "Can approve tasks"),
        )

    def __str__(self):
        return self.title


class TransactionHistory(models.Model):
    timestamp = models.DateTimeField(default=now)
    success = models.BooleanField(default=False)
    start_state = models.CharField(max_length=30)
    end_state = models.CharField(max_length=30)
    item = models.ForeignKey(TodoItem)

    def __str__(self):
        return '%s --- start state: %s --> %s (success: %s)' % (
            self.timestamp, self.start_state, self.end_state, self.success)