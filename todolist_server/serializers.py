from rest_framework import serializers
from todolist_server.models import TodoItem, State


class TodoItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = TodoItem


class StateSerializer(serializers.ModelSerializer):
    class Meta:
        model = State