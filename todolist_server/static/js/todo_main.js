var app = angular.module('rest-todo', ['ui.router','ui.bootstrap']);

app.constant('API_URL', window.location.origin + '/todo/api/todos/');
app.constant('REST_URL', window.location.origin + '/todo/changestate/');

app.config(function ($stateProvider, $urlRouterProvider, $httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken'
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken'

    $('[data-toggle="tooltip"]').tooltip();

    String.prototype.format = function () {
        var formatted = this;
        for (var arg in arguments) {
            formatted = formatted.replace("{" + arg + "}", arguments[arg]);
        }
        return formatted;
    };

    $stateProvider
        .state('home', {
            url: '/',
            templateUrl: '/static/templates/home_todo.html',
            controller: 'MainCtrl'
        })
        .state('edit-todo', {
            url: "/edit",
            templateUrl: '/static/templates/edit_todo.html',
            controller: 'MainCtrl'
        })
        .state('add-todo', {
            url: "/add",
            templateUrl: '/static/templates/add_todo.html',
            controller: 'MainCtrl'
        });

    $urlRouterProvider.otherwise('/');
});

app.controller('MainCtrl', function ($scope, $rootScope, Todos, $state, $filter) {
    $scope.newTodo = {};

    $scope.itemsPerPage = 5;
    $scope.currentPage = 1;

    $scope.addTodo = function () {
        Todos.addOne($scope.newTodo)
            .then(function (res) {
                // redirect to homepage once added
                $state.go('home');
            });
    };

    $scope.editTodo = function (todo) {
        Todos.update(todo).then(function (res) {
            // redirect to homepage once added
            $state.go('home');
        });
    }

    $scope.editCurrent = function (todo) {
        $rootScope.currentTodo = todo;
        console.log($scope.currentTodo);
        $state.go('edit-todo');
    }

    $scope.toggleFinished = function (todo) {
        Todos.update(todo);
    };

    $scope.deleteTodo = function (id) {
        Todos.delete(id);
        // update the list in ui
        $scope.todos = $scope.todos.filter(function (todo) {
            return todo.id !== id;
        })
    };

    $scope.changeState = function (id, state) {
        var currentId = id;
        Todos.changeState(id, state).then(function successCallback(response) {
            //$scope.todos[currentId].state = response.data;
            $scope.todos = $scope.todos.filter(function (todo) {
                if(todo.id == id) {
                    todo.state = response.data;
                }
                return true;
            })
        }, function errorCallback(response) {
            // error
            $("#modalNotAllowed").modal();
        });
    };

    $scope.paginatedListToDisplay = function() {
        $scope.filteredTodos = $filter('filter')($scope.todos, $scope.search);
        var begin = (($scope.currentPage - 1) * $scope.itemsPerPage);
        var end = begin + $scope.itemsPerPage;
        $scope.paginatedTodos = $scope.filteredTodos.slice(begin, end);
    };

    Todos.all().then(function (res) {
        $scope.filteredTodos = $scope.todos = res.data;
        $scope.paginatedListToDisplay();
    });
});

app.service('Todos', function ($http, API_URL, REST_URL) {
    var Todos = {};

    Todos.all = function () {
        return $http.get(API_URL);
    };

    Todos.update = function (updatedTodo) {
        return $http.put("{0}{1}/".format(API_URL, updatedTodo.id), updatedTodo);
    };

    Todos.delete = function (id) {
        return $http.delete("{0}{1}/".format(API_URL, id));
    };

    Todos.addOne = function (newTodo) {
        return $http.post(API_URL, newTodo)
    };

    Todos.changeState = function (id, state) {
        return $http.get("{0}{1}/{2}".format(REST_URL, id, state));
    }
    return Todos;
});




