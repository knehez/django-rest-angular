from django.conf.urls import include, url
from rest_framework import routers
from . import views

rest_router = routers.DefaultRouter()
rest_router.register(r'todos', views.TodoViewSet, base_name='todos')
rest_router.register(r'states', views.StateViewSet, base_name='states')

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url('^api/', include(rest_router.urls)),
    url(r'^changestate/(?P<pk>[0-9]+)/(?P<new_state>[a-z]+)$', views.change_state),
    url(r'^getstates/(?P<model>([A-z]|[a-z])+)/', views.get_states),
]