from django.shortcuts import render
from django_fsm import can_proceed, has_transition_perm
from django.shortcuts import get_object_or_404
from rest_framework import viewsets, status
from rest_framework.decorators import api_view
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from todolist_server.models import TodoItem, TransactionHistory, State
from todolist_server.serializers import TodoItemSerializer, StateSerializer

class TodoViewSet(viewsets.ModelViewSet):
    queryset = TodoItem.objects.all()
    serializer_class = TodoItemSerializer


class StateViewSet(viewsets.ModelViewSet):
    queryset = State.objects.all()
    serializer_class = StateSerializer


# Home route to send template to angular
def index(request):
    return render(request, 'todo_base.html')

def save_state_change(item, old_state, new_state, success):
    p = TransactionHistory.objects.create(success=success, start_state=old_state, end_state=new_state, item=item)
    p.save()

@api_view(['GET'])
def get_states(request, model):
    transitions = list(TodoItem().get_all_state_transitions())

    available_transitions = [[(t.name, t.custom) for t in transitions]]
    ordered_transactions = sorted(available_transitions[0], key=lambda item: item[1]['order'])
    return Response(ordered_transactions, status=status.HTTP_200_OK)


@api_view(['GET'])
def change_state(request, pk, new_state):
    todoitem = get_object_or_404(TodoItem, pk=pk)

    old_state = todoitem.state
    print("user:" + request.user.username)
    if not can_proceed(getattr(todoitem, new_state)) or not has_transition_perm(getattr(todoitem, new_state),
                                                                                request.user):
        save_state_change(todoitem, old_state, new_state, success=False)
        raise PermissionDenied

    getattr(todoitem, new_state)()

    todoitem.save()

    save_state_change(todoitem, old_state, new_state, success=True)

    return Response(todoitem.state, status=status.HTTP_200_OK)

